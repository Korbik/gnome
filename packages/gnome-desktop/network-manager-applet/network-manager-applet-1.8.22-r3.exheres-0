# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache
require option-renames [ renames=[ 'modem-manager wwan' ] ]

SUMMARY="A panel applet to configure networking via NetworkManager"
HOMEPAGE="http://www.gnome.org/projects/NetworkManager"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    wwan [[ description = [ support management of WWAN via Modem Manager ] ]]
    ( linguas: af an ar as ast be be@latin bg bn_IN bs ca ca@valencia crh cs da de dz el en_CA en_GB
               eo es et eu fa fi fr gd gl gu he hi hr hu id it ja kk km kn ko ku lt lv mk ml mr ms nb
               ne nl nn oc or pa pl pt pt_BR ro ru rw sk sl sq sr sr@latin sv ta te tg th tr ug uk ur
               vi wa zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.38]
        dev-libs/libsecret:1[>=0.18]
        gnome-desktop/gcr[>=3.14]
        gnome-desktop/libgudev[>=147]
        net-apps/NetworkManager[>=1.8]
        x11-libs/gtk+:3[>=3.10]
        x11-libs/libnotify[>=0.7]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        wwan? ( net-wireless/ModemManager )
        !net-libs/libnma [[
            description = [ libnma has been split out of nm-applet ]
            resolution = uninstall-blocked-before
        ]]
    run:
        sys-apps/dbus[>=1.2.6]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --enable-iso-codes
    --disable-mobile-broadband-provider-info #TODO: make an option once we have an exheres
    --with-gcr
    --without-appindicator
    --without-selinux
    --without-team
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' 'gtk-doc' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'wwan' )

src_install() {
    default
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

