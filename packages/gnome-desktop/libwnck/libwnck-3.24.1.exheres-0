# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Window Navigator Construction Key Library"
HOMEPAGE="http://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc gobject-introspection
    ( linguas: am ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs cy da de dz el
               en_CA en_GB en@shaw eo es et eu fa fi fr fy ga gl gu he hi hr hu id is it ja ka kk
               km kn ko ku ky li lt lv mai mi mk ml mn mr ms nb ne nl nn oc or pa pl pt pt_BR ro
               ru rw si sk sl sq sr sr@latin sv ta te th tr ug uk vi wa xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.6]
        sys-devel/gettext[>=0.19.4]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.32]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection?]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXres
        x11-libs/startup-notification[>=0.4]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.14] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-startup-notification' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gtk-doc 'gobject-introspection introspection' )

src_prepare() {
    edo sed -i '/AC_PATH_PROG(PKG_CONFIG/d' configure.ac

    edo intltoolize --force --automake
    edo autopoint --force
    autotools_src_prepare
}

