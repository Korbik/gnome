From 3eccba50409cd5efafa1519035a173b66f695ad1 Mon Sep 17 00:00:00 2001
From: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
Date: Sun, 19 Feb 2017 11:18:52 +0100
Subject: [PATCH 1/2] optionalise kerberos based authentication

Signed-off-by: Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
---
 meson.build                             | 11 +++++++++++
 meson_options.txt                       |  1 +
 panels/user-accounts/cc-realm-manager.c |  9 +++++++++
 panels/user-accounts/cc-realm-manager.h |  1 +
 panels/user-accounts/meson.build        |  9 ++++-----
 5 files changed, 26 insertions(+), 5 deletions(-)

diff --git a/meson.build b/meson.build
index f017d9c36..e60e3e662 100644
--- a/meson.build
+++ b/meson.build
@@ -190,6 +190,16 @@ endif
 config_h.set('HAVE_IBUS', enable_ibus,
              description: 'Defined if IBus support is enabled')
 
+# Kerberos support
+enable_kerberos = get_option('kerberos')
+if enable_kerberos
+    kerberos_deps = [
+      dependency('krb5')
+    ]
+endif
+config_h.set('HAVE_KERBEROS', enable_kerberos,
+             description: 'Defined if kerberos support is enabled')
+
 # thunderbolt
 config_h.set10('HAVE_FN_EXPLICIT_BZERO',
                cc.has_function('explicit_bzero', prefix: '''#include <string.h>'''),
@@ -294,6 +304,7 @@ output += ' Panels \n'
 output += '     GNOME Bluetooth (Bluetooth panel) .......... ' + host_is_linux_not_s390.to_string() + '\n'
 output += '     Cheese (Users panel webcam support) ........ ' + enable_cheese.to_string() + '\n'
 output += '     IBus (Region panel IBus support) ........... ' + enable_ibus.to_string() + '\n'
+output += '     Kerberos ................................... ' + enable_kerberos.to_string() + '\n'
 output += '     NetworkManager (Network panel) ............. ' + host_is_linux.to_string() + '\n'
 output += '     Wacom (Wacom tablet panel) ................. ' + host_is_linux_not_s390.to_string() + '\n'
 output += '     Snap support ............................... ' + enable_snap.to_string() + '\n'
diff --git a/meson_options.txt b/meson_options.txt
index e76308e8a..f555d120e 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -1,6 +1,7 @@
 option('cheese', type: 'boolean', value: true, description: 'build with cheese webcam support')
 option('documentation', type: 'boolean', value: false, description: 'build documentation')
 option('ibus', type: 'boolean', value: true, description: 'build with IBus support')
+option('kerberos', type: 'boolean', value: true, description: 'build with kerberos support')
 option('privileged_group', type: 'string', value: 'wheel', description: 'name of group that has elevated permissions')
 option('snap', type: 'boolean', value: false, description: 'build with Snap support')
 option('tests', type: 'boolean', value: true, description: 'build tests')
diff --git a/panels/user-accounts/cc-realm-manager.c b/panels/user-accounts/cc-realm-manager.c
index dd61c1312..83480728c 100644
--- a/panels/user-accounts/cc-realm-manager.c
+++ b/panels/user-accounts/cc-realm-manager.c
@@ -22,7 +22,9 @@
 
 #include "cc-realm-manager.h"
 
+#if defined(HAVE_KERBEROS)
 #include <krb5/krb5.h>
+#endif
 
 #include <glib.h>
 #include <glib/gi18n.h>
@@ -613,6 +615,7 @@ login_closure_free (gpointer data)
         g_slice_free (LoginClosure, login);
 }
 
+#if defined(HAVE_KERBEROS)
 static krb5_error_code
 login_perform_kinit (krb5_context k5,
                      const gchar *realm,
@@ -764,6 +767,7 @@ kinit_thread_func (GTask *task,
 
         g_object_unref (task);
 }
+#endif
 
 void
 cc_realm_login (CcRealmObject *realm,
@@ -796,7 +800,12 @@ cc_realm_login (CcRealmObject *realm,
         g_task_set_task_data (task, login, login_closure_free);
 
         g_task_set_return_on_cancel (task, TRUE);
+#if defined(HAVE_KERBEROS)
         g_task_run_in_thread (task, kinit_thread_func);
+#else
+        g_task_return_new_error (task, CC_REALM_ERROR, CC_REALM_ERROR_NOT_SUPPORTED,
+                                 _("kerberos based authentication support is disabled"));
+#endif
 
         g_object_unref (kerberos);
 }
diff --git a/panels/user-accounts/cc-realm-manager.h b/panels/user-accounts/cc-realm-manager.h
index 7e68e8e37..77910af26 100644
--- a/panels/user-accounts/cc-realm-manager.h
+++ b/panels/user-accounts/cc-realm-manager.h
@@ -29,6 +29,7 @@ typedef enum {
         CC_REALM_ERROR_BAD_PASSWORD,
         CC_REALM_ERROR_CANNOT_AUTH,
         CC_REALM_ERROR_GENERIC,
+        CC_REALM_ERROR_NOT_SUPPORTED,
 } CcRealmErrors;
 
 #define CC_REALM_ERROR (cc_realm_error_get_quark ())
diff --git a/panels/user-accounts/meson.build b/panels/user-accounts/meson.build
index b1c322a6b..1483c5b73 100644
--- a/panels/user-accounts/meson.build
+++ b/panels/user-accounts/meson.build
@@ -156,16 +156,11 @@ sources = common_sources + files(
   'um-fingerprint-dialog.c',
 )
 
-# Kerberos support
-krb_dep = dependency('krb5', required: false)
-assert(krb_dep.found(), 'kerberos libraries not found in your path')
-
 deps = common_deps + [
   accounts_dep,
   gdk_pixbuf_dep,
   gnome_desktop_dep,
   liblanguage_dep,
-  krb_dep,
   m_dep,
   polkit_gobject_dep,
   dependency('pwquality', version: '>= 1.2.2')
@@ -175,6 +170,10 @@ if enable_cheese
   deps += cheese_deps
 endif
 
+if enable_kerberos
+  deps += kerberos_deps
+endif
+
 cflags += [
   '-DGNOMELOCALEDIR="@0@"'.format(control_center_localedir),
   '-DHAVE_LIBPWQUALITY',
-- 
2.25.1

