# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache python [ blacklist=2 multibuild=false ] meson

SUMMARY="A tool to customize GNOME 3 options."
HOMEPAGE="https://wiki.gnome.org/action/show/Apps/Tweaks"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.16]
    build+run:
        gnome-desktop/gsettings-desktop-schemas[>=3.33.0]
        gnome-bindings/pygobject:3[>=3.10][python_abis:*(-)?]
        x11-libs/gtk+:3[>=3.14.0][gobject-introspection]
    run:
        dev-libs/libhandy
        gnome-desktop/gnome-desktop:3.0[gobject-introspection]
        gnome-desktop/gnome-shell[>=3.24]
        gnome-desktop/gnome-settings-daemon
        gnome-desktop/libsoup:2.4[gobject-introspection]
        gnome-desktop/mutter
        x11-libs/libnotify[gobject-introspection]
        x11-libs/pango[gobject-introspection]
    suggestion:
        gnome-desktop/gnome-shell-extensions [[
            description = [ The user-theme extension is required for setting gnome-shell themes ]
        ]]
"

src_prepare() {
    meson_src_prepare

    # use the python version set via python_abis
    edo sed -e "s:/usr/bin/env python3:/usr/$(exhost --target)/bin/env ${PYTHON}:" -i ${PN}

    # use correct python libdir
    edo sed "/python3.sysconfig_path('purelib'))/d" -i \
        meson.build
    edo sed -e "16s:^:pythondir = '$(python_get_libdir)'\n:" -i \
        meson.build
}

src_compile() {
    python_disable_pyc
    meson_src_compile
}

src_install() {
    meson_src_install
    python_bytecompile
}

