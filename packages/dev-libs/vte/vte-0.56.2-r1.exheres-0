# Copyright 2008 Stephen Bennett <spb@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic
require gnome.org [ suffix=tar.xz ] vala [ vala_dep=true with_opt=true ]

SUMMARY="A terminal emulator widget"
HOMEPAGE="https://developer.gnome.org/arch/gnome/widgets/vte.html"

LICENCES="GPL-2"
SLOT="2.91"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gnutls [[ description = [ Enable gnutls support (required for writing data encrypted to disk) ] ]]
    gobject-introspection
    gtk-doc
    vapi [[ requires = gobject-introspection ]]
    ( linguas: am ang ar as ast az be be@latin bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr fur ga gl gu he hi hr hu id is it ja ka kn ko ku ky
               li lt lv mai mi mk ml mn mr ms nb nds ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl
               sq sr sr@latin sv ta te th tr ug uk uz@cyrillic vi wa xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools
        dev-util/intltool[>=0.40.0]
        dev-lang/perl:*[>=5.8.1]
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        virtual/pkg-config[>=0.19]
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        dev-libs/glib:2[>=2.40.0]
        dev-libs/pcre2[>=10.21]
        gnome-desktop/gobject-introspection:1[>=0.9.0]
        sys-libs/zlib
        x11-libs/gtk+:3[>=3.20.0][gobject-introspection?]
        x11-libs/pango[>=1.22.0][gobject-introspection?]
        gnutls? ( dev-libs/gnutls[>=3.2.7] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-deprecation' '--disable-static' '--with-gtk=3.0' '--with-iconv' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'gnutls' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' 'gobject-introspection introspection' 'vapi vala' )

if [[ $(exhost --target) == *-musl* ]]; then
    DEFAULT_SRC_PREPARE_PATCHES=(
        "${FILES}"/0001-Add-W_EXITCODE-macro-for-non-glibc-systems.patch
    )
fi

src_prepare() {
    edo gtkdocize
    edo intltoolize
    default
}

