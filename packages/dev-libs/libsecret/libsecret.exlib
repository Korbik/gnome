# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true ]

SUMMARY="Secret Service dbus client library"
HOMEPAGE="https://wiki.gnome.org/Libsecret"

LICENCES="LGPL-2"
SLOT="1"
MYOPTIONS="debug gobject-introspection gtk-doc
    vapi [[ requires = gobject-introspection ]]
    (
        linguas:
            an ar as be bs ca ca@valencia cs da de el eo es eu fr fur gl he hu
            id it ja kk ko lt lv ml nb nl pa pl pt pt_BR ru sk sl sr sr@latin sv
            tg tr uk zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        app-doc/gtk-doc-autotools
        sys-devel/gettext[>=0.19.8]
        sys-devel/libtool
        virtual/pkg-config
        (
            app-text/docbook-xml-dtd:4.2
            app-text/docbook-xsl-stylesheets
            dev-libs/libxslt
        ) [[ description = [ For man pages ] ]]
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.2.2]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.29] )
        !gnome-desktop/libsecret:0 [[
            description = [ slot move ]
            resolution = uninstall-blocked-after
        ]]
"

# requires Xorg
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-gcrypt'
    '--enable-manpages'
    "--with-libgcrypt-prefix=/usr/$(exhost --target)"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'debug'
    'gobject-introspection introspection'
    'gtk-doc'
    'vapi vala'
)

