# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="Actions, Menus and Toolbars Kit for GTK+ applications"

LICENCES="LGPL-2.1"
SLOT="5"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    ( linguas: cs da de en_GB es eu fr fur hu id it ja lt nl pl pt_BR ro ru sl sr sv tr uk )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        dev-libs/glib:2[>=2.52]
        x11-libs/gtk+:3[>=3.22.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.42.8] )
"

BUGS_TO="keruspe@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-code-coverage
    --disable-installed-tests
    --disable-valgrind
    --disable-Werror
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' gtk-doc )

src_install() {
    default

    ! option gtk-doc && edo rm -rf "${IMAGE}"/usr/share/gtk-doc
}

